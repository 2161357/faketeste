package com.example.sergi.faketeste.model;

import java.util.LinkedList;

public enum Classes {
    INSTANCIA;
    private LinkedList<UsersManager> modelities;

    Classes() {

        modelities=new LinkedList<>();
    }

    public void add(UsersManager modelity){
        modelities.add(modelity);
    }

    public LinkedList<UsersManager> getModelities(){
        return modelities;
    }

    public UsersManager getModelity(String name){
        for(UsersManager usersManager : modelities){
            if(usersManager.getName().equals(name)){
                return usersManager;
            }
        }

        return null;
    }

    public void clearAllContacts(){
        modelities = new LinkedList<>();
    }

}
