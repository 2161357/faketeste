package com.example.sergi.faketeste.model;

import java.util.LinkedList;

public class UsersManager {
    private String name;
    private LinkedList<User> users;
    public UsersManager(String name) {
        this.name = name;
        this.users=new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<User> getUsers() {
        return users;
    }

    public void setUsers(LinkedList<User> users) {
        this.users = users;
    }

    public void addUser(User user){
        users.add(user);
    }

    public String toString(){
        return name;
    }


    public boolean hasUsers(){
        return !users.isEmpty();
    }


}
