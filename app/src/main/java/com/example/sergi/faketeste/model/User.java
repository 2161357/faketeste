package com.example.sergi.faketeste.model;

import java.util.LinkedList;

public class User {
    private String nome;
    private int idade;

    public User(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    @Override
    public String toString() {
        return nome;
    }

}
