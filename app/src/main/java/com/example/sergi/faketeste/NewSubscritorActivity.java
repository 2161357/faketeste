package com.example.sergi.faketeste;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.sergi.faketeste.model.Classes;
import com.example.sergi.faketeste.model.User;
import com.example.sergi.faketeste.model.UsersManager;

public class NewSubscritorActivity extends AppCompatActivity {
    private EditText editName;
    private EditText editAge;
    private CheckBox ckAbs;
    private CheckBox ckJump;
    private CheckBox ckPilates;
    private CheckBox ckCycling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Classes.INSTANCIA.add(new UsersManager("ABS"));
        Classes.INSTANCIA.add(new UsersManager("Jump"));
        Classes.INSTANCIA.add(new UsersManager("Pilates"));
        Classes.INSTANCIA.add(new UsersManager("Cycling"));

        editName=findViewById(R.id.subscription_name);
        editAge=findViewById(R.id.subscription_age);
        ckAbs=findViewById(R.id.subscription_abs);
        ckJump=findViewById(R.id.subscription_Jump);
        ckPilates=findViewById(R.id.subscription_Pilates);
        ckCycling=findViewById(R.id.subscription_Cycling);

        editName.setText("");
        editAge.setText("");
        ckAbs.setChecked(false);
        ckJump.setChecked(false);
        ckPilates.setChecked(false);
        ckCycling.setChecked(false);
    }

    public void newSubscription(View view) {
        if(editName.getText().toString()==""){
            return;
        }

        if(editAge.getText().toString()=="" || Integer.parseInt(editAge.getText().toString())<=15){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Sorry! We only accept registrations for those over 15 years of age!");
            builder.show();

            return ;
        }

        User user = new User(editName.getText().toString(), Integer.parseInt(editAge.getText().toString()));

        UsersManager usersManager;
        if(ckAbs.isChecked()){
            usersManager = Classes.INSTANCIA.getModelity("ABS");
            usersManager.addUser(user);
        }
        if(ckJump.isChecked()){
            usersManager =Classes.INSTANCIA.getModelity("Jump");
            usersManager.addUser(user);
        }
        if(ckPilates.isChecked()){
            usersManager =Classes.INSTANCIA.getModelity("Pilates");
            usersManager.addUser(user);
        }
        if(ckCycling.isChecked()){
            usersManager =Classes.INSTANCIA.getModelity("Cycling");
            usersManager.addUser(user);
        }

        startActivity(ListClassesActivity.getIntent(this));
    }
}
