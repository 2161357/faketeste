package com.example.sergi.faketeste;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.sergi.faketeste.model.Classes;
import com.example.sergi.faketeste.model.User;
import com.example.sergi.faketeste.model.UsersManager;

import java.util.LinkedList;

public class ListClassesActivity extends AppCompatActivity {
    private Spinner list_classes;
    private Spinner list_participants;

    public static Intent getIntent(Context context) {
        return new Intent(context, ListClassesActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_classes);


        setTitle(R.string.classes_name_activity);

        list_classes=findViewById(R.id.list_classes);
        list_participants=findViewById(R.id.list_participants);


        LinkedList<UsersManager> classes = Classes.INSTANCIA.getModelities() ;

        ArrayAdapter<UsersManager> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, classes);

        list_classes.setAdapter(adapter);

        list_classes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = parent.getItemAtPosition(position).toString();

                UsersManager usersManager =Classes.INSTANCIA.getModelity(selectedItem);
                addListValue(usersManager);

            } // to close the onItemSelected
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
        list_participants.setEmptyView(list_classes);
    }

    private void addListValue(UsersManager usersManager) {
        LinkedList<User> users = usersManager.getUsers() ;
        ArrayAdapter<User> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, users);

        list_participants.setAdapter(adapter);
    }

}
