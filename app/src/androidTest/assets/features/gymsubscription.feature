Feature: Subscription to the gym
  As a user
  I want to enter my name, age and the classes ABS, Jump, Pilates and Cycling
  So that I can apply to a subscription of the gym

  Background:
    Given I see an empty subscription form

  Scenario: Valid subscription
    When I fill in the field name with 'Filipa'
    And I fill in the field age with '35'
    And I select the 'ABS' class
    And I click on the Subscribe button
    Then I see a new screen with the classes list

  Scenario: Empty fields
    When I fill in the field name with 'Ana'
    And I click on the Subscribe button
    Then I see the error message 'Please fill in the empty fields!'