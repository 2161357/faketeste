Feature: List of classes and participants in each class
  As a user
  I want to access the classes list screen
  So that I can see the participants of each class

  Background:
    Given I see the classes list
    And I see an empty participants list

  Scenario: Select a class from the list
    When I select the class ‘ABS’ from the list
    Then I see a participants list without data