package steps;

import com.example.sergi.faketeste.R;
import com.mauriciotogneri.greencoffee.GreenCoffeeSteps;
import com.mauriciotogneri.greencoffee.annotations.Given;
import com.mauriciotogneri.greencoffee.annotations.Then;
import com.mauriciotogneri.greencoffee.annotations.When;

public class GymSubscriptionSteps extends GreenCoffeeSteps {
    @Given("^I see an empty subscription form$")
    public void i_see_an_empty_subscription_form() {
        onViewWithId(R.id.subscription_name).isEmpty();
        onViewWithId(R.id.subscription_age).isEmpty();
        onViewWithId(R.id.subscription_abs).isNotChecked();
        onViewWithId(R.id.subscription_Jump).isNotChecked();
        onViewWithId(R.id.subscription_Pilates).isNotChecked();
        onViewWithId(R.id.subscription_Cycling).isNotChecked();
    }

    @When("^I fill in the field name with 'Filipa'$")
    public void i_fill_in_the_field_name_with_Filipa() {
        onViewWithId(R.id.subscription_name).type("Filipa");
        closeKeyboard();
    }

    @When("^I fill in the field age with '(\\d+)'$")
    public void i_fill_in_the_field_age_with(int arg1) {
        onViewWithId(R.id.subscription_age).type(Integer.toString(arg1));
        closeKeyboard();
    }

    @When("^I select the 'ABS' class$")
    public void i_select_the_ABS_class() {
        onViewWithText("ABS").click();
    }

    @When("^I click on the Subscribe button$")
    public void i_click_on_the_Subscribe_button() {
        onViewWithId(R.id.subscription_save).click();
    }

    @Then("^I see a new screen with the classes list$")
    public void i_see_a_new_screen_with_the_classes_list() {
        onViewWithText(R.string.classes_name_activity).isDisplayed();
        onViewWithId(R.id.list_classes).isDisplayed();
        onViewWithId(R.id.list_classes).isNotEmpty();
    }

    @When("^I fill in the field name with 'Maria'$")
    public void i_fill_in_the_field_name_with_Maria() {
        onViewWithId(R.id.subscription_name).type("Maria");
        closeKeyboard();
    }

    @When("^I select the 'Cycling' modality$")
    public void i_select_the_Cycling_modality() {
        onViewWithText("Cycling").click();
    }

    @Then("^I see the error message 'Sorry! We only accept registrations for those over (\\d+) years of age!'$")
    public void i_see_the_error_message_Sorry_We_only_accept_registrations_for_those_over_years_of_age(int arg1) {
        onViewWithText("Sorry! We only accept registrations for those over " +Integer.toString(arg1)+" years of age!").isDisplayed();
    }

}
