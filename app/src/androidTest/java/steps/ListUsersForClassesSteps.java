package steps;

import com.example.sergi.faketeste.R;
import com.example.sergi.faketeste.model.Classes;
import com.example.sergi.faketeste.model.User;
import com.example.sergi.faketeste.model.UsersManager;
import com.mauriciotogneri.greencoffee.GreenCoffeeSteps;
import com.mauriciotogneri.greencoffee.annotations.Given;
import com.mauriciotogneri.greencoffee.annotations.Then;
import com.mauriciotogneri.greencoffee.annotations.When;

import static org.junit.Assert.assertFalse;

public class ListUsersForClassesSteps extends GreenCoffeeSteps {
    @Given("^I see the classes list$")
    public void i_see_the_classes_list() {
        onViewWithId(R.id.list_classes).isDisplayed();
    }

    @Given("^I see an empty participants list$")
    public void i_see_an_empty_participants_list() {
        onViewWithId(R.id.list_participants).isNotDisplayed();
    }

    @When("^I select the class ‘ABS’ from the list$")
    public void i_select_the_class_ABS_from_the_list() {

        onViewWithText("ABS").click();
    }

    @Then("^I see a participants list without data$")
    public void i_see_a_participants_list_without_data() {
        UsersManager usersManager = Classes.INSTANCIA.getModelity("ABS");
        assertFalse(usersManager.hasUsers());


        for(User user : usersManager.getUsers()){
            onViewWithText(user.getNome()).isNotDisplayed();
        }
    }
}
