package features;

import android.support.test.rule.ActivityTestRule;

import com.example.sergi.faketeste.ListClassesActivity;
import com.example.sergi.faketeste.model.Classes;
import com.example.sergi.faketeste.model.UsersManager;
import com.mauriciotogneri.greencoffee.GreenCoffeeConfig;
import com.mauriciotogneri.greencoffee.GreenCoffeeTest;
import com.mauriciotogneri.greencoffee.Scenario;
import com.mauriciotogneri.greencoffee.ScenarioConfig;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import steps.ListUsersForClassesSteps;

@RunWith(Parameterized.class)
public class ListUsersForClassesFeatureTest extends GreenCoffeeTest {
    @Rule
    public ActivityTestRule<ListClassesActivity> activity=new ActivityTestRule<>(ListClassesActivity.class);


    public ListUsersForClassesFeatureTest(ScenarioConfig scenario) {
        super(scenario);
    }

    @Parameterized.Parameters(name="{0}")
    public static List<ScenarioConfig> data() throws IOException {
        return new GreenCoffeeConfig()
                .withFeatureFromAssets("assets/features/listUsers.feature")
                .scenarios();
    }


    @Override
    protected void beforeScenarioStarts(Scenario scenario, Locale locale) {

        Classes.INSTANCIA.add(new UsersManager("ABS"));
        Classes.INSTANCIA.add(new UsersManager("Jump"));
        Classes.INSTANCIA.add(new UsersManager("Pilates"));
        Classes.INSTANCIA.add(new UsersManager("Cycling"));
    }

    @Test
    public void listUsersForClassTest(){
        start(new ListUsersForClassesSteps());
    }




}
