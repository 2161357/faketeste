package features;

import android.support.test.rule.ActivityTestRule;

import com.example.sergi.faketeste.NewSubscritorActivity;
import com.example.sergi.faketeste.model.Classes;
import com.example.sergi.faketeste.model.UsersManager;
import com.mauriciotogneri.greencoffee.GreenCoffeeConfig;
import com.mauriciotogneri.greencoffee.GreenCoffeeTest;
import com.mauriciotogneri.greencoffee.Scenario;
import com.mauriciotogneri.greencoffee.ScenarioConfig;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Locale;

import steps.GymSubscriptionSteps;

@RunWith(Parameterized.class)
public class GymSubscriptionTest extends GreenCoffeeTest {

    @Rule
    public ActivityTestRule<NewSubscritorActivity> activity = new ActivityTestRule<>(NewSubscritorActivity.class);

    public GymSubscriptionTest(ScenarioConfig scenario) {
        super(scenario);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<ScenarioConfig> data() throws IOException {
        return new GreenCoffeeConfig().withFeatureFromAssets("assets/features/gymsubscription.feature")
                .scenarios();
    }

    @Test
    public void newSubscritorTest() {

        start(new GymSubscriptionSteps());
    }

    @Before
    protected void set_up(Scenario scenario, Locale locale) {
        Classes.INSTANCIA.add(new UsersManager("ABS"));
        Classes.INSTANCIA.add(new UsersManager("Jump"));
        Classes.INSTANCIA.add(new UsersManager("Pilates"));
        Classes.INSTANCIA.add(new UsersManager("Cycling"));
    }
/*
    @Override
    protected void beforeScenarioStarts(Scenario scenario, Locale locale) {
        Classes.INSTANCIA.clearAllContacts();
        Classes.INSTANCIA.add(new UsersManager("ABS"));
        Classes.INSTANCIA.add(new UsersManager("Jump"));
        Classes.INSTANCIA.add(new UsersManager("Pilates"));
        Classes.INSTANCIA.add(new UsersManager("Cycling"));
    }*/

}
